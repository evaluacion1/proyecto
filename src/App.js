//import logo from './logo.svg';
import logoU from './logo.png';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logoU} className="App-logo" alt="logo" />
        <p>
          Orlando Gutiérrez Ramírez!
        </p>
        <a
          className="App-link"
          href="https://www.ute.edu.mx/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Visita el portal UTEQ
        </a>
      </header>
    </div>
  );
}

export default App;
